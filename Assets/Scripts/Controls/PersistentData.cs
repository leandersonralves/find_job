using UnityEngine;
using MessagePack;
#if UNITY_WSA
using UnityEngine.Windows;
#else
using System.IO;
#endif

namespace Gaminho
{
    [MessagePackObject]
    public class PersistentData
    {
        [Key(0)]
        public int level;

        public static void Save (PersistentData persistentData)
        {
            var data = MessagePackSerializer.Serialize(persistentData);
            File.WriteAllBytes(
                string.Concat(Application.persistentDataPath, "/save_findjob.data"),
                data
            );
        }

        public static PersistentData Load ()
        {
            var data = File.ReadAllBytes(
                string.Concat(Application.persistentDataPath, "/save_findjob.data")
            );

            return MessagePackSerializer.Deserialize<PersistentData>(data);
        }

        public static bool HasSave ()
        {
            return File.Exists(string.Concat(Application.persistentDataPath, "/save_findjob.data"));
        }
    }
}