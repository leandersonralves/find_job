﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;

    public Transform BarLife;
    public GameObject UIshield;
    public Transform BarShield;

    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
        if (Statics.WithShield)
        {
            UIshield.gameObject.SetActive(true);
            BarShield.localScale = new Vector3((float)Statics.LifeShield / 10f, 1, 1);
        }
        else
        {
            UIshield.gameObject.SetActive(false);
        }

#if FIND_JOB_CHEAT
        if (Input.GetKeyDown(KeyCode.P)) { LevelPassed(); }
#endif
    }

    private bool isPaused = false;

    private float cacheScale = 1f;

    private void Pause (bool isPaused)
    {
        this.isPaused = isPaused;
        if (isPaused)
        {
            cacheScale = Time.timeScale;
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = cacheScale;
        }
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }

    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        var persistentData = new PersistentData();
        persistentData.level = Statics.CurrentLevel;
        PersistentData.Save(persistentData);

        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }

    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }
}
