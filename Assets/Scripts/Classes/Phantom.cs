using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Transform that replies a position/rotation delayed.
/// </summary>
public class Phantom : MonoBehaviour
{
    public struct State
    {
        public Vector3 position;
        public Quaternion rotation;
        public bool isMotorOn;
        public bool hasShield;
        public float timeStamp;
    }

    [SerializeField]
    private float delay = 2f;

    private Queue<State> queuePosRot = new Queue<State>();

    private Transform cacheTransform;

    [SerializeField]
    private GameObject shield;

    [SerializeField]
    private GameObject motor;

    private void Awake()
    {
        cacheTransform = transform;
    }

    private void Update()
    {
        if (queuePosRot.Count < 1) return;

        State nextStep = queuePosRot.Peek();

        //Enqueue until reach the time delayed.
        while (Time.time > nextStep.timeStamp && queuePosRot.Count > 2)
            nextStep = queuePosRot.Dequeue();

        cacheTransform.position = nextStep.position;
        cacheTransform.rotation = nextStep.rotation;
        motor.SetActive(nextStep.isMotorOn);
        shield.SetActive(nextStep.hasShield);
    }

    /// <summary>
    /// Register a position/rotation in actual Time.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    public void RegisterTransform(Vector3 position, Quaternion rotation, bool isMotorOn, bool hasShield)
    {
        State tc = new State();
        tc.position = position;
        tc.rotation = rotation;
        tc.isMotorOn = isMotorOn;
        tc.hasShield = hasShield;
        //Register time stamp delayed.
        tc.timeStamp = Time.time + delay;
        queuePosRot.Enqueue(tc);
        //Debug.Log("Registering pos " + position + " " + rotation + " mot " + isMotorOn + " shield " + hasShield + " t " + tc.timeStamp);
    }
}
